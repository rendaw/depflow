from setuptools import setup

setup(
    name='depflow',
    version='0.0.6',
    author='rendaw',
    url='https://github.com/rendaw/depflow',
    license='BSD',
    py_modules=['depflow'],
)
